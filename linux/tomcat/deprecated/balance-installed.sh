#!/bin/bash
set -e
## 读取配置文件
. ../config/my-config.sh

## 运行单机安装
#./single-installed.sh

##4. 修改软件的相关配置文件，本地化或优化配置；
cp -f balance-server-8181.xml $TOMCAT_HOME/conf/server.xml

##7. 修改防火墙配置，以便外部访问；
##../common/change-iptables.sh 45564

## 重启
service $TOMCAT_INID start