#!/bin/bash
set -e
## 如果文件不存在，则下载
if [ ! -f "$JDK_FILE" ]
then
    wget $DOWNLOAD_URL/$JDK_FILE
fi

if [ ! -d "$JDK_HOME" ]
then
    echo "ehll"
    mkdir -p "$JDK_HOME"
fi
##安装
rpm -i --badreloc --relocate /usr/java=/home/java $JDK_FILE

##写入环境变量 
JAVA_PROFILE=$SYS_PROFILE_DIR/java.sh
echo "" > $JAVA_PROFILE
echo "JAVA_HOME=$JDK_HOME" >> $JAVA_PROFILE
echo "CLASSPATH=.:\$JAVA_HOME/lib/dt.jar:\$JAVA_HOME/lib/tools.jar" >> $JAVA_PROFILE
echo "PATH=$PATH:\$JAVA_HOME/bin" >> $JAVA_PROFILE
echo "export JAVA_HOME CLASSPATH PATH" >> $JAVA_PROFILE
##修改文件权限
chmod 755 $JAVA_PROFILE
##刷新环境变量
source /etc/profile
##查看是否安装成功
java -version
