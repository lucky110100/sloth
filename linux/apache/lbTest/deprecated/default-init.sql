-- create user
CREATE USER if not exists '${dbUser1}'@'%' IDENTIFIED BY '${dbPwd1}';
GRANT All ON *.* TO '${dbUser1}'@'%';
flush privileges;

create database if not exists bufferdb character set = 'utf8' collate = 'utf8_general_ci';