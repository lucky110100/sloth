#!/bin/bash
set -e
## 卸载已经安装的软件rpm方式的
## 卸载mysql,jdk,apache
for haha in $(rpm -qa | grep -E 'mysql|jdk|http' )
do 
	mkdir -p /home/logs
	echo "uninstall $haha !" >> /home/logs/uninstall-rpm.log
	rpm -e --nodeps $haha
	done