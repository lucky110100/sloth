#!/bin/bash
## 生成秘钥，一路回车默认即可
ssh-keygen -t rsa
## 将上面生成的id_rsa.pub文件拷贝至usr2@remote的~/.ssh/authorized_keys文件中
cat ~/.ssh/id_rsa.pub | ssh usr2@remote 'cat >> .ssh/authorized_keys'
cat ~/.ssh/id_rsa.pub | ssh root@192.168.1.112 'cat >> .ssh/authorized_keys'
##测试
ssh -t -p 22 root@192.168.1.112 "cd /home; echo  haha >> demo.txt"
ssh root@192.168.1.112 "cd /home; echo  haha >> demo.txt"
ssh root@192.168.1.113 "cd /home; echo  haha >> demo.txt"
## 下载脚本，遍历目录下载且不创建主目录
wget -c -r -nH -np -k -L -p http://192.168.1.200:9300
## 设置权限
chmod a+x . -R


原因分析，以及处理步骤：
1  查看 log/secure，分析问题在何处；检查/var/log/messages

2  查看 /root/.ssh/authorized_keys文件的属性，以及.ssh文件属性   是不是权限过大。.ssh目录的权限必须是700，同时本机的私钥的权限必须设置成600：

3  修改/etc/ssh/sshd_config文件,  把密码认证关闭, 将认证改为 passwordAuthentication no   重启下sshd。 service sshd restart;

4  执行setenforce 0,暂时关闭selinux