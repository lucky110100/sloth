#!/bin/bash
set -e
## 读取配置文件
. ../config/my-config.sh

##1.下载安装包，并上传到服务器；
NGINX_URL=$URL_FILE/nginx
wget $NGINX_URL/$NGINX_FILE

##2. 解压安装包到指定目录，修改文件的读写执行权限；
tar -zxvf $NGINX_FILE -C /home

##3. 编译源文件，并进行安装；

##4. 修改软件的相关配置文件，本地化或优化配置；
cp -f $NGINX_CNF $NGINX_HOME/conf/nginx.conf

##5. 修改用户环境变量，或建立软链接，方便快速调用软件命令；

##6. 把软件注册为服务，并随系统启动；
cp -f $NGINX_INID /etc/init.d/$NGINX_INID
chmod a+x /etc/init.d/$NGINX_INID
## 先替换服务文件中的一些内容
sed -i "s#@{nginxhome}#$NGINX_HOME#g" /etc/init.d/$NGINX_INID
chkconfig $NGINX_INID on

##7. 修改防火墙配置，以便外部访问；
../common/change-iptables.sh $NGINX_PORT

##8. 设置监控日志和备份目录，方便维护；
ln -sf $NGINX_HOME/logs/error.log $HOME_LOG/nginx.err 
ln -sf $NGINX_HOME/logs/access.log $HOME_LOG/nginx.acc

##9. 能够安全干净删除已安装软件，并重新安装
echo 请编写卸载脚本！