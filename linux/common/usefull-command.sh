#!/bin/bash
echo "I want to do something ..."
# 查看系统信息
cat /proc/meminfo 
# Linux查看当前操作系统版本信息
cat /proc/version
# Linux查看cpu相关信息，包括型号、主频、内核信息等
cat /proc/cpuinfo 
# 查看内核/操作系统/CPU信息
uname -a 
# Linux查看版本说明当前CPU运行在32bit模式下， 但不代表CPU不支持64bit
getconf LONG_BIT 
# 查看环境变量资源  
env 
# 查看各分区使用情况 
df -h 
# 查看所有网络接口的属性 
ifconfig 
# 查看防火墙设置 
iptables -L 
#查看防火墙文件
cat /etc/sysconfig/iptables
# 查看当前用户的计划任务服务 
crontab -l 
# 列出所有系统服务
chkconfig -–list  
# 列出所有启动的系统服务程序 
chkconfig -–list | grep on 
# 查看所有安装的软件包
rpm -qa 
# 查看服务运行状态
service --status-all 
# 查看路由表 
route -n 
# 查看所有监听端口
netstat -lntp  
# 查看所有已经建立的连接
netstat -antp
# 下载文件

