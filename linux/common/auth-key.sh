#!/bin/bash
set -e
#在主机上配置免密登陆（root）

## 检查输入参数
if [ $# -lt 1 ]; then
    echo "$0 错误：请传入参数，(1、类型:name/node；2、地址：域名或IP)！"
	echo "例如：$0 create name00 或copy 192.168.1.2"
	exit 2
fi
cd ~/.ssh
if [ "$1" == "name" ];then
	if [ ! -f 'id_dsa.pub' ];then
		ssh-keygen -t dsa -P '' -f id_dsa
		cat id_dsa.pub >> authorized_keys
	fi	
elif [ "$1" == "node" ];then
	scp authorized_keys root@${2}:~/.ssh
else
	echo '跳过授权'
fi