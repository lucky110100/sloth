#!/bin/bash
set -e
##解决方案实例
##说明：搭建一个高性能高并发的视频分享和管理网站

##有N台相似的Linux服务器，无外网访问能力
##需要安装和实现以下软件或服务：
##jdk，Apache，tomcat，ftp，mysql，redis，nginx
##其中实现应用服务器负载均衡，mysql数据库主从同步并定期清理日志表，
##ngigx流媒体服务，redis缓存服，webapp批量部署和同步和文件备份，监控日志管理服务
##
## 1.填写服务器连接信息

## 2.循环远程服务器执行jdk安装脚本

## 3.主服务器执行Apache，ftp安装脚本

## 4.循环远程服务器执行tomcat负载均衡安装脚本

## 5.循环远程服务器执行nginx集群安装脚本

## 6.循环远程服务器执行mysql主从安装脚本

## 7.循环远程服务器执行webapp应用部署和文件同步脚本

## 8.循环远程服务器执行监控日志管理脚本


#0.填写服务器连接信息
# hosts=(192.168.1.112 192.168.1.113)
# users=(root root)
# pwd=(654321 654321)

#创建公钥，建立信任关系(手动)
# read -p "是否创建公钥，建立过的可以跳过(Y/N):" isY
# echo $isY
# if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	# echo $isY
# fi
# echo $isY
# read -p "是否创建公钥，建立过的可以跳过(Y/N)2:" isY
# echo $isY
# if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	# echo $isY
# fi
# echo $isY

# cat > demo.txt <<END
# JAVA_HOME=$JAVA_HOME
# CLASSPATH=.:\$JAVA_HOME/lib/dt.jar:\$JAVA_HOME/lib/tools.jar
# PATH=$PATH:\$JAVA_HOME/bin
# export JAVA_HOME CLASSPATH PATH
# END
## 读取配置文件
. ../config/my-config.sh
## 向配置文件中输入基本信息
cat > my.cnf <<END
[mysqld]
basedir = $MYSQL_HOME
datadir = $MYSQL_DATA
port = $MYSQL_PORT
server_id = $1
# socket = .....

sql_mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES
## 修改临时表大小，提高查询速度
tmp_table_size=300M
max_heap_table_size=500M
## 最大连接数
max_connections=768
## 表名大小写不敏感
lower_case_table_names=1

## 日志配置
log_error=@{mydata}/mysql.err
#log=@{mydata}/mysql.log
slow-query-log=on
slow-query-log-file=@{mydata}/mysql.slow
long_query_time=3
END

## 写入初始化文件
cat > init.sql <<END
CREATE USER if not exists '${dbUser1}'@'%' IDENTIFIED BY '${dbPwd1}';
GRANT All ON *.* TO '${dbUser1}'@'%';
flush privileges;
create database if not exists bufferdb character set = 'utf8' collate = 'utf8_general_ci';
END

## 主从选择
if [ "$2" == "master" ];then
	cat >> my.cnf <<END
## 主从配置-主库
log-bin=mysql-bin
## 需要同步的数据库
#binlog-do-db=db1 
#binlog-do-db=db2
#binlog-do-db=db3
## 不需要同步的数据库
#binlog-ignore-db=mysql
END
	echo init.sql >> "GRANT REPLICATION SLAVE ON *.* to '${mySyncUser}'@'%' identified by '${mySyncPwd}'; ";
elif [ "$2" == "slave" ];then
	cat >> my.cnf <<END
## 主从配置-从库
log_bin = mysql-bin
relay_log = mysql-relay-bin
log_slave_updates = 1
read_only = 1
## 需要复制的库
#replicate-do-db=db1
## 不需要复制的库
#replicate-ignore-db=mysql
END
	cat >> init.sql <<END
STOP SLAVE;
CHANGE MASTER TO MASTER_HOST='${hosts[0]}', MASTER_PORT=${MYSQL_PORT}, MASTER_USER='${mySyncUser}', MASTER_PASSWORD='${mySyncPwd}', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=154, MASTER_CONNECT_RETRY=60;
START SLAVE;
END
fi


## 检查tomcat
cd /home/tomcat/tomcat-8181/webapps
cat /home/tomcat/tomcat-8181/conf/server.xml
ps ax | grep java
netstat -lntp
service tomcat7 start
service tomcat7 stop
cd /home/tomcat/tomcat-8181/webapps/

rm -rf /home/tomcat/tomcat-8181/webapps/report.war

 wget http://10.68.43.211:9200/webapps/report.war
wget http://192.168.1.111:9200/webapps/report.war

vi /home/tomcat/tomcat-8181/webapps/report/WEB-INF/classes/

tail -f /home/tomcat/tomcat-8181/logs/catalina.out

tail -f /ltdata/logs/clicklog

tail -f /home/tomcat/tomcat-8989-test/logs/catalina.out 


## 检查apahce
cd /home/apache/conf/


#mysql冷备份
service mysqld stop
cd /home
tar zcvf mysql-241.tar.gz mysql/
service mysqld start
mv mysql-241.tar.gz /home/install/mysql

wget http://192.168.1.111:9200/mysql/mysql-241.tar.gz

service mysqld stop
rm -rf /home/mysql
tar zxvf mysql-241.tar.gz -C /home


