#!/bin/bash
## 说明：Hadoop-2.4.0家族环境自动化部署安装脚本
## 作者：yaodh
## 博客：http://www.cnblogs.com/lucky110100
## 码云：http://git.oschina.net/signup?inviter=lucky110100
set -e
## 检查输入参数
if [ $# -lt 3 ]; then
    echo "$0 错误：请传入参数，(1、类型:name/node;2、机器名：name00/node01;3、机器IP)！"
	echo "例如：$0 name name00 127.0.0.1"
	exit 2
fi
echo "**************"
echo "参数确认： 类型：$1   机器名： $2    机器IP：$3"
echo "**************"
#read -p "继续/跳过(Y/N):" isY
#if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
#	echo '					开始安装……'
#else 
	#	echo '					退出安装！！！'
	#	exit 2
#fi

if [ ! -f 'hadoop.info'  ];then
	touch hadoop.info
fi
#写入集群信息——很重要
sed -i "/$2/d" hadoop.info
echo "$1	$2	$3" >> hadoop.info

URL_SHELL=http://172.16.5.204:9300

read -p "SSH链接慢，或自动添加到know_hosts？(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	ssh root@$3 "sed -i 's@#UseDNS yes@UseDNS no@g' /etc/ssh/sshd_config;sed -i 's@#   GSSAPIAuthentication no@GSSAPIAuthentication no@g' /etc/ssh/ssh_config;sed -i 's@#   StrictHostKeyChecking ask@StrictHostKeyChecking no@g' /etc/ssh/ssh_config;service sshd restart"
else 
	echo '					跳过……'
fi

read -p "授权公钥……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	wget $URL_SHELL/common/auth-key.sh;chmod a+x auth-key.sh;./auth-key.sh $1 $3
else 
	echo '					跳过……'
fi

read -p "下载脚本……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	##下载脚本，并删除已rpm安装的软件
	ssh root@$3 "mkdir -p /home/install"
	## 下载脚本
	#ssh root@$3 "cd /home/install;wget -N -r -nH -np -k -L -p $URL_SHELL" #层级下载有点问题
	## 下载脚本包
	ssh root@$3 "cd /home/install;rm -f linux.zip;wget $URL_SHELL/linux.zip"
	## 解压并提权
	ssh root@$3 "cd /home/install;unzip -o linux.zip;chmod a+x /home/install -R"
else 
	echo '					跳过……'
fi

read -p "删除已rpm安装的软件……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	## 删除rpm安装的软件
	ssh root@$3 "cd /home/install/common;./uninstall-rpm.sh"
else 
	echo '					跳过……'
fi

read -p "安装java……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	ssh root@$3 "cd /home/install/java;./installed-jdk.sh"
else 
	echo '					跳过……'
fi

read -p "安装mysql……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	ssh root@$3 "cd /home/install/mysql;./installed-mysql.sh 1 single"
else 
	echo '					跳过……'
fi

read -p "安装hadoop……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	ssh root@$3 "cd /home/install/hadoop;./hadoop-single.sh $1 $2 $3"
else
	echo '					跳过……'
fi

read -p "立即加入Hadoop集群……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	./hadoop-cluster.sh
else 
	echo '					跳过……'
fi

read -p "安装hive……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	ssh root@$3 "cd /home/install/hadoop;./hive-single.sh"
else
	echo '					跳过……'
fi

read -p "安装zookeeper……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	ssh root@$3 "cd /home/install/hadoop;./zk-single.sh"
else 
	echo '					跳过……'
fi

read -p "集群zookeeper……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	./zk-cluster.sh
else 
	echo '					跳过……'
fi

read -p "安装hbase……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	ssh root@$3 "cd /home/install/hadoop;./hbase-single.sh $1 $2 $3"
else 
	echo '					跳过……'
fi

read -p "集群hbase……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	./hbase-cluster.sh
else 
	echo '					跳过……'
fi

read -p "设置环境变量……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	ssh root@$3 "cd /home/install/config;./my-profile.sh;source /etc/profile"
	ssh root@$3 "source /etc/profile"
else 
	echo '					跳过……'
fi