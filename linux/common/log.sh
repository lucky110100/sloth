#!/bin/bash
LOG_TOMCAT=/home/tomcat/apache-tomcat-7.0.37/logs/catalina.out
log(){
  tail -f $LOG_TOMCAT
}
lognx(){
  tail -f /u01/tools/logs/catalina.out.$1
}
if [ $# -eq 1 ]
then
  if [ $1 = "log" ]
  then
    log `date +%Y-%m-%d`
  elif [ $1 = "lognx" ]
  then
    lognx `date +%Y-%m-%d`
  else
    log $1
  fi
elif [ $# -eq 2 ]
then
  lognx $1 $2
else
  echo "please input parameters again"
fi
