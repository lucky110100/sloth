#!/bin/bash
#set -e
#判断是否为超级管理员
if [ $UID -ne 0 ];
then
    echo "Please run with super user"
    exit 1
fi
###############系统级的####################
##系统版本
OS_VERSION=Centos-6.3x64
## 一些常用的系统文件或目录
## 1. 两个文件都是设置环境变量文件的，/etc/profile是永久性的环境变量,是全局变量，/etc/profile.d/设置所有用户生效
## 2. /etc/profile.d/比/etc/profile好维护，不想要什么变量直接删除/etc/profile.d/下对应的shell脚本即可，不用像/etc/profile需要改动此文件
SYS_PROFILE=/etc/profile
SYS_PROFILE_DIR=/etc/profile.d
###############全局变量####################
YESTERDAY=$(date -d last-day +%Y-%m-%d)
TODAY=$(date +%Y-%m-%d)
echo $YESTERDAY $TODAY

## 下载路径
URL_FILE=http://172.16.5.204:9200
URL_SHELL=http://172.16.5.204:9300

##备份路径
BACKUP_DIR=/home/backup
mkdir -p $BACKUP_DIR
##安装包路径
HOME_INSTALL=/home/install
mkdir -p $HOME_INSTALL
## 全局日志路径
HOME_LOG=/home/logs
mkdir -p $HOME_LOG
## 全局数据路径
HOME_DATA=/home/data
mkdir -p $HOME_DATA
## 安装日志
LOG_INSTALL=$HOME_LOG/LOG_INSTALL_$TODAY.log
#填写服务器连接信息
arrndx=(0 1 2 3)
hosts=(192.168.1.111 192.168.1.112 192.168.1.113 192.168.1.114)
users=(root root root root)
pwd=(654321 654321 654321 654321)

############### MYSQL ####################
MYSQL_PUBLISH=mysql-5.7.13-linux-glibc2.5-x86_64.tar.gz
MYSQL_VERSION=mysql-5.7.13
MYSQL_FILE=mysql-241.tar.gz
#MYSQL_FILE=mysql-5.7.13.tar.gz
MYSQL_HOME=/home/mysql
MYSQL_BACK=/home/backup/mysql
MYSQL_CNF=default-my.cnf
MYSQL_INID=mysqld
MYSQL_DATA=/home/mysql/data
MYSQL_PORT=3306
# 初始化一个mysql管理员用户和一个主从同步用户
dbUser1=haha
dbPwd1=xixi@haha.com
mySyncUser=mySync
mySyncPwd=xixi@haha.com

############### JDK ####################
JDK_FILE=jdk1.7.0_79.tar.gz
JAVA_HOME=/home/java/jdk1.7.0_79

################# TOMCAT ####################
TOMCAT_FILE=apache-tomcat-7.0.37-nodoc.tar.gz
TOMCAT_VERSIOIN=apache-tomcat-7.0.37
TOMCAT_HOME=/home/tomcat
TOMCAT_CNF=server-8181.xml
TOMCAT_INID=tomcat7-2g
TOMCAT_PORT=8181
TOMCAT_CATALINA='catalina.sh'
JAVA_OPTS="-Xms1024m -Xmx2048m -XX:PermSize=512m -XX:MaxNewSize=512m  -XX:MaxPermSize=1024m"
################# APACHE ####################
APACHE_FILE=apache-2.2.31.tar.gz
APACHE_HOME=/home/apache
APACHE_CNF=httpd.conf
APACHE_INID=httpd
APACHE_PORT=8080
################# NGINX ####################
NGINX_VERSION=nginx-1.10.0
NGINX_FILE=nginx-1.10.0-installed.tar.gz
NGINX_HOME=/home/local/nginx
NGINX_BACK=/home/backup/nginx
NGINX_CNF=nginx.conf.linux2
NGINX_INID=nginx
NGINX_PORT=9200
################# REDIS ####################
REDIS_VERSION=redis--3.2.5
REDIS_FILE=redis-3.2.5-bin.tar.gz
REDIS_HOME=/home/local/redis
REDIS_BACK=/home/backup/redis
REDIS_CNF=redis.conf.linux2
REDIS_INID=redis
REDIS_PORT=9200
################# FTP ####################
FTP_FILE=vsftpd-2.2.2-11.el6_4.1.x86_64.rpm

################# HADOOP ####################
#HADOOP_FILE=hadoop-2.2.0.tar.gz
#HADOOP_VERSION=hadoop-2.2.0
#HADOOP_HOME=/home/soft/hadoop-2.2.0
HADOOP_FILE=hadoop-2.4.0-64bit.tar.gz
HADOOP_VERSION=hadoop-2.4.0
HADOOP_DIR_OWN=/home/hadoop
HADOOP_DIR_INSTALL=$HADOOP_DIR_OWN/src
HADOOP_HOME=$HADOOP_DIR_INSTALL/$HADOOP_VERSION
HADOOP_DIR_DATA=$HOME_DATA/hadoop
HADOOP_MASTER=name20401
HADOOP_MASTER_IP=172.16.204.1
HADOOP_PID_DIR=$HOME_DATA/hadoop/pids
HADOOP_DIR_CONF=$HADOOP_HOME/etc/hadoop

HIVE_FILE=apache-hive-1.2.1-bin.tar.gz
HIVE_VERSION=apache-hive-1.2.1-bin
HIVE_HOME=/home/hadoop/src/apache-hive-1.2.1-bin
HIVE_LOGS=$HOME_LOG/hive

ZOO_FILE=zookeeper-3.4.5.tar.gz
ZOO_VERSION=zookeeper-3.4.5
ZOO_HOME=/home/hadoop/src/zookeeper-3.4.5
ZOO_DATA=$HOME_DATA/zookeeper
ZOO_LOGS=$HOME_LOG/zookeeper

HBASE_FILE=hbase-0.98.21-hadoop2-bin.tar.gz
HBASE_VERSION=hbase-0.98.21-hadoop2
HBASE_HOME=/home/hadoop/src/hbase-0.98.21-hadoop2
HBASE_DATA=$HOME_DATA/hbase
HBASE_LOGS=$HOME_LOG/hbase
HBASE_PID_DIR=$HBASE_DATA/pids
#HBASE_ZOOKEEPER_QUORUM=node11703,node11704,node11901,node11902,node11903

PHOENIX_FILE=apache-phoenix-4.8.0-HBase-0.98-bin.tar.gz
PHOENIX_VERSION=apache-phoenix-4.8.0-HBase-0.98-bin
PHOENIX_HOME=/home/hadoop/src/apache-phoenix-4.8.0-HBase-0.98-bin
PHOENIX_LOGS=$HOME_LOG/phoenix
