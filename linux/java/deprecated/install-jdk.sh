#!/bin/bash
set -e
## 读取配置文件
. ../config/my-config.sh
## 如果文件不存在，则下载
if [ ! -f "$JDK_FILE" ]
then
    wget $DOWNLOAD_URL/$JDK_FILE
fi

if [ ! -d "$JAVA_HOME" ]
then
    echo "Make dir $JAVA_HOME!"
    mkdir -p "$JAVA_HOME"
fi
## tar -zxvf 解压并列出详情; -C 指定目录
tar -zxf $JDK_FILE -C $JAVA_HOME
##写入环境变量 
JAVA_PROFILE=$SYS_PROFILE_DIR/java.sh
echo "" > $JAVA_PROFILE
echo "JAVA_HOME=$JAVA_HOME" >> $JAVA_PROFILE
## 转义字符"\"
echo "CLASSPATH=.:\$JAVA_HOME/lib/dt.jar:\$JAVA_HOME/lib/tools.jar" >> $JAVA_PROFILE
echo "PATH=$PATH:\$JAVA_HOME/bin" >> $JAVA_PROFILE
echo "export JAVA_HOME CLASSPATH PATH" >> $JAVA_PROFILE
##修改文件权限
chmod 755 $JAVA_PROFILE
##刷新环境变量
source /etc/profile
##查看是否安装成功
java -version