#!/bin/bash
set -e
##1.下载安装包，并上传到服务器；
wget http://192.168.1.200:9200/apache/apr-1.5.2.tar.gz
wget http://192.168.1.200:9200/apache/apr-util-1.5.4.tar.gz
wget http://192.168.1.200:9200/apache/httpd-2.2.31.tar.gz
wget http://192.168.1.200:9200/apache/tomcat-connectors-1.2.41-src.tar.gz
##2. 解压安装包到指定目录，修改文件的读写执行权限；
tar -zxvf apr-1.5.2.tar.gz
tar -zxvf apr-util-1.5.4.tar.gz
tar -zxvf httpd-2.2.31.tar.gz
tar -zxvf tomcat-connectors-1.2.41-src.tar.gz
##3. 编译源文件，并进行安装；
# centos6.5已经安装apr，apr-util，调到httpd-2.2.31
cd apr-1.5.2
./configure --prefix=/home/local/apr
make
make install
cd ..

cd apr-util-1.5.4
./configure --prefix=/home/local/apr-util  --with-apr=../apr-1.5.2
make
make install
cd ..

cd httpd-2.2.31
#./configure --prefix=/home/apache --enable-so --enable-mods-shared=most --with-mpm=worker   --with-apr=../apr-1.5.2   --with-apr-util=../apr-util-1.5.4 
./configure --prefix=/home/apache --enable-so --enable-mods-shared=most --with-mpm=worker
make
make install
cd ..

cd tomcat-connectors-1.2.41-src/native 
./configure --prefix=/home/local/tomcat-connectors-1.2.32 --with-apxs=/home/apache/bin/apxs
make
#make install
cp apache-2.0/mod_jk.so /home/apache/modules/ 
cd ..
##4. 修改软件的相关配置文件，本地化或优化配置；
cp -f httpd.conf /home/apache/conf/httpd.conf
##5. 修改用户环境变量，或建立软链接，方便快速调用软件命令；
#ln -sf /home/bin/haha /usr/bin
##6. 把软件注册为服务，并随系统启动；
cp -f httpd /etc/init.d/httpd
chmod a+x /etc/init.d/httpd
chkconfig httpd on
##7. 修改防火墙配置，以便外部访问；
../common/change-iptables.sh 9000
## 负载均衡配置
cp -f httpd.conf.balance /home/apache/conf/httpd.conf
cp -f workers.properties /home/apache/conf/workers.properties
cp -f uriworkermap.properties /home/apache/conf/uriworkermap.properties

cd /home/tomcat/
cp -rf apache-tomcat-7.0.37/ tomcat7-s1
cp -rf apache-tomcat-7.0.37/ tomcat7-s2
cd /home/install/linux/tomcat/
cp server-8181.xml /home/tomcat/tomcat7-s1/conf/server.xml
cp server-8282.xml /home/tomcat/tomcat7-s2/conf/server.xml

cp -rf lbTest/ /home/tomcat/tomcat7-s1/webapps/
cp -rf lbTest/ /home/tomcat/tomcat7-s2/webapps/

/home/tomcat/tomcat7-s1/bin/startup.sh
/home/tomcat/tomcat7-s2/bin/startup.sh
service httpd start

service httpd stop
/home/tomcat/tomcat7-s1/bin/shutdown.sh
/home/tomcat/tomcat7-s2/bin/shutdown.sh

## 负载均衡+集群配置
#!/bin/bash
../common/change-iptables.sh 45564
cd /home/install/linux/apache
cp -f httpd.conf.balance /home/apache/conf/httpd.conf
cp -f workers.properties /home/apache/conf/workers.properties
cp -f uriworkermap.properties /home/apache/conf/uriworkermap.properties

cp -rf lbTest/ /home/tomcat/tomcat7-s1/webapps/
cp -rf lbTest/ /home/tomcat/tomcat7-s2/webapps/
cp -f tomcat-cluster-8181.xml /home/tomcat/tomcat7-s1/conf/server.xml
cp -f tomcat-cluster-8282.xml /home/tomcat/tomcat7-s2/conf/server.xml

/home/tomcat/tomcat7-s1/bin/startup.sh
/home/tomcat/tomcat7-s2/bin/startup.sh
service httpd start

service httpd stop
/home/tomcat/tomcat7-s1/bin/shutdown.sh
/home/tomcat/tomcat7-s2/bin/shutdown.sh