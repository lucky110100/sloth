#!/bin/bash
## 读取配置文件
. ../config/my-config.sh
#停止服务
service mysqld stop
#删除客户端连接
rm -rf /usr/bin/mysql
#删除安装文件夹
rm -rf $MYSQL_HOME
#删除系统服务文件
rm -rf /etc/init.d/mysqld
#删除控制文件
rm -rf /etc/my.cnf

#删除用户组和用户
#usermod –G mysql mysql
userdel mysql
groupdel mysql
