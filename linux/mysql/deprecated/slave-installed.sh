#!/bin/bash
set -e
## 读取配置文件
. ../config/my-config.sh
## 先执行单机安装
#./single-installed.sh
## 复制从库配置文件到指定目录
cp -f slave-my.cnf /etc/my.cnf
sed -i "s#@{myhome}#$MYSQL_HOME#g" /etc/my.cnf
sed -i "s#@{mydata}#$MYSQL_DATA#g" /etc/my.cnf
sed -i "s#@{myport}#$MYSQL_PORT#g" /etc/my.cnf
sed -i "s#@{serverid}#1#g" /etc/my.cnf
## 删除从库的uuid，否则启动从服务报错 because master and slave have equal MySQL server UUIDs;
rm -f $MYSQL_HOME/data/auto.cnf
## 启动数据库，并执行初始化脚本
service mysqld restart
#mysql -u root --auto-rehash --line-numbers --show-warnings mysql < slave-init.sql