#!/bin/bash
## 说明：Hadoop-2.4.0家族环境自动化部署安装脚本
## 作者：yaodh
## 博客：http://www.cnblogs.com/lucky110100
## 码云：http://git.oschina.net/signup?inviter=lucky110100
set -e



## 检查输入参数
if [ $# -lt 3 ]; then
    echo "错误：请传入参数，(1、类型:name/node;2、机器名：name00/node01;3、机器IP)！"
	echo "例如：$0 name name00 127.0.0.1"
	exit 2
fi

URL_SHELL=http://172.16.5.204:9300
MTYPE=$1
MNAME=$2
MIP=$3

function checkInfo {
	
	echo "********************************************************"
	echo "新节点： 类型：$MTYPE   机器名： $MNAME    机器IP：$MIP"
	echo "********************************************************"

	echo "********************************************************"
	echo "hadoop集群信息："
	cat hadoop.info || true
	echo "********************************************************"
	echo "********************************************************"
	echo "zookeeper集群信息："
	cat zk.info || true
	echo "********************************************************"
	echo "********************************************************"
	echo "hbase集群信息："
	cat hbase.info || true
	echo "********************************************************"
}

function writeInfo {
	if [ ! -f 'hadoop.info'  ];then
		touch hadoop.info
	fi
	sed -i "/$MNAME/d" hadoop.info
	echo "$MTYPE	$MNAME	$MIP" >> hadoop.info
	
	if [ ! -f 'zk.info'  ];then
		touch zk.info
	fi
	sed -i "/$MNAME/d" zk.info
	echo "$MTYPE	$MNAME	$MIP" >> zk.info
	
	if [ ! -f 'hbase.info'  ];then
		touch hbase.info
	fi
	sed -i "/$MNAME/d" hbase.info
	echo "$MTYPE	$MNAME	$MIP" >> hbase.info
	
	checkInfo
}

function menu {
  echo
  echo -e "\t\t准备安装"
  echo -e "\t0. 退出"
  echo -e "\t1. 检查集群信息"
  echo -e "\t2. 写入集群信息"
  echo -e "\t3. 解决SSH链接慢，并自动添加到know_hosts"
  echo -e "\t4. 授权公钥"
  echo -e "\t5. 下载脚本"
  echo -e "\t6. 删除rpm安装的软件"
  echo -e "\t7. 安装jdk"
  echo -e "\t8. 安装mysql"
  echo -e "\t9. 安装hadoop"
  echo -e "\t10. 集群Hadoop"
  echo -e "\t11. 安装hive"
  echo -e "\t12. 安装zookeeper"
  echo -e "\t13. 集群zookeeper"
  echo -e "\t14. 安装hbase"
  echo -e "\t15. 集群hbase"
  echo -e "\t16. 设置环境变量"
  read -p "请输入数字选择：" option
}

while [ 1 ]
do
 menu

case $option in
0)
   break ;;
1)
	checkInfo
	;;
2)
	writeInfo
	;;
3)
	ssh root@$MIP "sed -i 's@#UseDNS yes@UseDNS no@g' /etc/ssh/sshd_config;sed -i 's@#   GSSAPIAuthentication no@GSSAPIAuthentication no@g' /etc/ssh/ssh_config;sed -i 's@#   StrictHostKeyChecking ask@StrictHostKeyChecking no@g' /etc/ssh/ssh_config;service sshd restart"
	;;
4)
	wget $URL_SHELL/common/auth-key.sh;chmod a+x auth-key.sh;./auth-key.sh $MTYPE $MIP
	;;
5)
	ssh root@$MIP "mkdir -p /home/install"
	ssh root@$MIP "cd /home/install;rm -f linux.zip;wget $URL_SHELL/linux.zip"
	ssh root@$MIP "cd /home/install;unzip -o linux.zip;chmod a+x /home/install -R"
	;;
6)
	ssh root@$MIP "cd /home/install/common;./uninstall-rpm.sh"
	;;
7)
	ssh root@$MIP "cd /home/install/java;./installed-jdk.sh"
	;;
8)
	ssh root@$MIP "cd /home/install/mysql;./installed-mysql.sh 1 single"
	;;
9)
	ssh root@$MIP "cd /home/install/hadoop;./hadoop-single.sh $MTYPE $MNAME $MIP"
	;;
10)
	./hadoop-cluster.sh
	;;
11)
	ssh root@$MIP "cd /home/install/hadoop;./hive-single.sh"
	;;
12)
	ssh root@$MIP "cd /home/install/hadoop;./zk-single.sh"
	;;
13)
	./zk-cluster.sh
	;;
14)
	ssh root@$MIP "cd /home/install/hadoop;./hbase-single.sh $MTYPE $MNAME $MIP"
	;;
15)
	./hbase-cluster.sh
	;;
16)
	ssh root@$MIP "cd /home/install/config;./my-profile.sh;source /etc/profile"
	ssh root@$MIP "source /etc/profile"
	;;
 *)
   echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>对不起，没有该选项！" ;;
 esac
# echo -en "按任意键继续："
# read -n 1 line
done
