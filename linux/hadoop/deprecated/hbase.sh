#!/bin/bash
set -e
## 读取配置文件
. ../config/my-config.sh
## 重新设置下载链接
HBASE_URL=$URL_FILE/hadoop
mkdir -p $HBASE_HOME

## 如果文件不存在，则下载
if [ ! -f "$HBASE_FILE" ]
then
    wget $HBASE_URL/$HBASE_FILE
fi
rm -rf $HBASE_HOME
tar -zxvf $HBASE_FILE -C /home/hadoop/src
##写入环境变量 
HBASE_PROFILE=$SYS_PROFILE_DIR/hbase.sh

## 块输入，转义字符"\"
cat > $HBASE_PROFILE <<END
HBASE_HOME=$HBASE_HOME
## PATH=/usr/lib64/qt-3.3/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
PATH=\$PATH:\$HBASE_HOME/bin:\$HBASE_HOME/conf
export HBASE_HOME PATH
END

##修改文件权限
chmod 755 $HBASE_PROFILE
##刷新环境变量
source /etc/profile

## 设置配置文件
cp -f $HBASE_VERSION/hbase-env.sh $HBASE_HOME/conf
cp -f $HBASE_VERSION/hbase-site.xml $HBASE_HOME/conf
cp -f $HBASE_VERSION/regionservers $HBASE_HOME/conf
## 替换变量
sed -i "s@\${javaHome}@${JAVA_HOME}@g" $HBASE_HOME/conf/hbase-env.sh
sed -i "s@\${zooData}@${ZOO_DATA}@g" $HBASE_HOME/conf/hbase-site.xml

## 重新设置目录的拥有者
chown hadoop:hadoop /home/hadoop -R

## 同步子节点
# node01
scp $HBASE_PROFILE root@node01:/etc/profile.d
scp -r $HBASE_HOME root@node01:/home/hadoop/src
ssh root@node01 "chown hadoop:hadoop /home/hadoop -R"
sudo -u hadoop ssh hadoop@node01 "source /etc/profile;env"

# node02
scp $HBASE_PROFILE root@node02:/etc/profile.d
scp -r $HBASE_HOME root@node02:/home/hadoop/src
ssh root@node02 "chown hadoop:hadoop /home/hadoop -R"
sudo -u hadoop ssh hadoop@node02 "source /etc/profile;env"
