@echo off
:start
echo ============请输入数字选择下一步操作:
echo ============1：ip设置为172.16.133.2
echo ============2：ip设置为172.16.77.2
echo ============3：设置为DHCP自动获取ip
set /P var=":"
if %var%==1 goto ip10
if %var%==2 goto ip172
if %var%==3 goto ipdhcp
:ip10
cls
netsh interface ip set address "本地连接" static 172.16.133.2 255.255.0.0 172.16.0.1 1
cmd /c netsh interface ip set dns "本地连接" static 202.96.134.133
echo **IP设置为172.16.133.2，设置成功**
echo ------------------------------------------
goto start
:ip172
cls
netsh interface ip set address "本地连接" static 172.16.77.2 255.255.0.0 172.16.0.1 1
cmd /c netsh interface ip set dns "本地连接" static 8.8.4.4
echo **IP设置为172.16.77.2，设置成功**
echo ------------------------------------------
goto start
:ipdhcp
cls
netsh interface ip set address "本地连接" dhcp
netsh interface ip delete dns "本地连接" all
ipconfig /flushdns
echo **IP设置为DHCP获取，设置成功**
echo ------------------------------------------
goto start