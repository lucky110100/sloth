#!/bin/bash
set -e
1.安装openssl-fips-2.0.2.tar.gz

[root@localhost mrms]# tar -zxvf openssl-fips-2.0.2.tar.gz 

[root@localhost mrms]# cd openssl-fips-2.0.2

[root@localhost openssl-fips-2.0.2]# ./config 

[root@localhost openssl-fips-2.0.2]# make

[root@localhost openssl-fips-2.0.2]# make install
2.安装zlib-1.2.7.tar.gz

[root@localhost mrms]# tar -zxvf zlib-1.2.7.tar.gz

[root@localhost mrms]# cd zlib-1.2.7

[root@localhost zlib-1.2.7]# ./configure 

[root@localhost zlib-1.2.7]# make

[root@localhost zlib-1.2.7]# make install
3.安装pcre-8.21.tar.gz

[root@localhost mrms]# tar -zxvf pcre-8.21.tar.gz

[root@localhost mrms]# cd pcre-8.21

[root@localhost pcre-8.21]# ./configure 

[root@localhost pcre-8.21]# make

[root@localhost pcre-8.21]# make install
 4.安装 nginx-1.2.6.tar.gz

[root@localhost mrms]# tar -zxvf nginx-1.2.6.tar.gz 

[root@localhost mrms]# cd nginx-1.2.6

[root@localhost nginx-1.2.6]# ./configure --with-pcre=../pcre-8.21 --with-zlib=../zlib-1.2.7 --with-openssl=../openssl-fips-2.0.2

[root@localhost nginx-1.2.6]# make

[root@localhost nginx-1.2.6]# make install
至此Nginx的安装完成!

第三步:检测是否安装成功

[root@localhost nginx-1.2.6]# cd  /usr/local/nginx/sbin

[root@localhost sbin]# ./nginx -t



2、为nginx脚本添加权限

	chmod a+x /etc/init.d/nginx

	3、添加nginx服务
	chkconfig --add nginx
	chkconfig nginx on
检查编译环境
which gcc
或
whereis gcc
没有就
yum install gcc-c++

make unistall
make distclean
./configure --prefix=/home/local

wget http://192.168.1.200:9300/nginx/zlib-1.2.8.tar.gz
tar -zxvf zlib-1.2.8.tar.gz
cd zlib-1.2.8
./configure --prefix=/home/local/zlib
make
make install
cd ..
wget http://192.168.1.200:9300/nginx/pcre-8.38.tar.gz
tar -zxvf pcre-8.38.tar.gz
cd pcre-8.38
./configure --prefix=/home/local/pcre
make
make install
cd ..
wget http://192.168.1.200:9300/nginx/nginx-1.10.0.tar.gz
tar -zxvf nginx-1.10.0.tar.gz
cd nginx-1.10.0
./configure --prefix=/home/local/nginx --with-pcre=../pcre-8.38 --with-zlib=../zlib-1.2.8  --with-http_flv_module --with-http_mp4_module --with-http_stub_status_module
##./configure --prefix=/usr/local/nginx --with-pcre=../pcre-8.38 --with-zlib=../zlib-1.2.8  --with-http_flv_module --with-http_mp4_module --with-http_stub_status_module
make
make install
/usr/local/nginx/sbin/nginx -t

/home/local/nginx/sbin/nginx -t

sed -i "/-A INPUT -j REJECT --reject-with icmp-host-prohibited/i -A INPUT -m state --state NEW -m tcp -p tcp --dport 9200 -j ACCEPT" /etc/sysconfig/iptables