#!/bin/bash
set -e

echo "系统环境检查和配置……"

interactive
## 检查输入参数
if [ $# -lt 1 ]; then
    echo "$0 错误：请传入参数，(1、静默安装 Y/N )！"
	echo "例如：$0 Y 或 $0 n"
	exit 2
fi
flag = false
isY = no
if [ $1 == "y"] || ["$1" == "Y"];then
	flag = true
	isY = y
fi

flag || read -p "继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	echo '					执行……'
else 
	echo '					跳过……'
fi
