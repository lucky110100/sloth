################# 设置自定义环境变量 ####################
. ./my-config.sh
##写入环境变量 
MY_PROFILE=$SYS_PROFILE_DIR/zzmy-profile.sh
## 块输入，转义字符"\"

cat > $MY_PROFILE <<END
JAVA_HOME=$JAVA_HOME
HADOOP_HOME=$HADOOP_HOME
HIVE_HOME=$HIVE_HOME
ZOO_HOME=$ZOO_HOME
HBASE_HOME=$HBASE_HOME
MYSQL_HOME=$MYSQL_HOME
PATH=/usr/lib64/qt-3.3/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
PATH=\$PATH::\$JAVA_HOME/bin:\$HADOOP_HOME/bin:\$HIVE_HOME/bin:\$ZOO_HOME/bin:\$HBASE_HOME/bin:\$HIVE_HOME/bin:\$MYSQL_HOME/bin
export HADOOP_COMMON_LIB_NATIVE_DIR=${HADOOP_HOME}/lib/native  
export HADOOP_OPTS="-Djava.library.path=${HADOOP_HOME}/lib" 
export JAVA_HOME HADOOP_HOME HIVE_HOME ZOO_HOME HBASE_HOME MYSQL_HOME PATH
END

##修改文件权限
chmod 755 $MY_PROFILE
##刷新环境变量
#source /etc/profile