#!/bin/bash
## 读取配置文件
. ../config/my-config.sh

for i in ${arrndx[@]};do
  echo ${hosts[$i]}
done

if ["$1" == "delmaster"];then
	echo "删除已经安装的软件……"
	service $MYSQL_INID stop
	rm -rf $MYSQL_HOME
	
	rm -rf $JAVA_HOME
	
	service $TOMCAT_INID stop
	rm -rf $TOMCAT_HOME
	
	service $APACHE_INID stop
	rm -rf $APACHE_HOME
	
	service $NGINX_INID stop
	rm -rf $NGINX_HOME
	echo "删除完毕"
elif ["$1" == "apachelb"];then
	for i in ${arrndx[@]};do
		echo ${hosts[$i]}
	done
fi

## 1..开始安装
read -p "是否删除安装(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	service $MYSQL_INID stop
	rm -rf $MYSQL_HOME
	
	rm -rf $JAVA_HOME
	
	service $TOMCAT_INID stop
	rm -rf $TOMCAT_HOME
	
	service $APACHE_INID stop
	rm -rf $APACHE_HOME
	
	service $NGINX_INID stop
	rm -rf $NGINX_HOME
fi

if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	for i in {1..3};do
		count=$(($i+1))
	## 1.1 下载脚本，并删除已rpm安装的软件
		ssh ${users[$i]}@${hosts[$i]} "mkdir -p /home/install"
		## 下载脚本
		ssh ${users[$i]}@${hosts[$i]} "cd /home/install;wget -N -r -nH -np -k -L -p $URL_SHELL"
		## 提权
		ssh ${users[$i]}@${hosts[$i]} "chmod a+x /home/install -R"
		## 删除rpm安装的软件
		ssh ${users[$i]}@${hosts[$i]} "cd /home/install/common;./uninstall-rpm.sh"

	## 2.循环远程服务器执行jdk安装脚本
		ssh ${users[$i]}@${hosts[$i]} "cd /home/install/java;./installed-jdk.sh"

	## 4.循环远程服务器执行tomcat负载均衡安装脚本
		ssh ${users[$i]}@${hosts[$i]} "cd /home/install/tomcat;./single-installed.sh;./cluster-installed.sh"

	## 6.循环远程服务器执行mysql主从安装脚本
		ssh ${users[$i]}@${hosts[$i]} "cd /home/install/mysql;./installed-mysql.sh $count single"

	## 7.循环远程服务器执行webapp应用部署和文件同步脚本

	## 8.循环远程服务器执行监控日志管理脚本

	done
fi

