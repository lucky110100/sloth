#!/bin/bash
set -e
## 读取配置文件
. ../config/my-config.sh
## 重新设置下载链接
HBASE_URL=$URL_FILE/hadoop

## 如果文件不存在，则下载
if [ ! -f "$HBASE_FILE" ]
then
    wget $HBASE_URL/$HBASE_FILE
fi
rm -rf $HBASE_HOME
tar -zxvf $HBASE_FILE -C ${HADOOP_DIR_OWN}/src

## 设置配置文件
cat > hbase-env.sh <<END
export HBASE_OPTS="-XX:+UseConcMarkSweepGC"
export JAVA_HOME=${JAVA_HOME}
export HBASE_MANAGES_ZK=false
export HBASE_LOG_DIR=$HBASE_LOGS
export HBASE_PID_DIR=$HBASE_PID_DIR
END

cp -f hbase-env.sh $HBASE_HOME/conf

mkdir -p $HBASE_DATA;chown -R hadoop:hadoop $HBASE_DATA
mkdir -p $HBASE_LOGS;chown -R hadoop:hadoop $HBASE_LOGS

