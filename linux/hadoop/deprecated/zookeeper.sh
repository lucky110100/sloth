#!/bin/bash
set -e
## 检查输入参数
if [ $# -lt 1 ]; then
    echo "$0 错误：请传入参数，(1、参数:未使用)！"
	echo "例如：$0 haha"
	exit 2
fi

#######################	2.安装hadoop 	#######################
## 读取配置文件
. ../config/my-config.sh
## 重新设置下载链接
ZOO_URL=$URL_FILE/hadoop
mkdir -p $ZOO_HOME

## 如果文件不存在，则下载
if [ ! -f "$ZOO_FILE" ]
then
    wget $ZOO_URL/$ZOO_FILE
fi
rm -rf $ZOO_HOME
tar -zxvf $ZOO_FILE -C /home/hadoop/src
##写入环境变量 
ZOO_PROFILE=$SYS_PROFILE_DIR/zoo.sh

## 块输入，转义字符"\"
cat > $ZOO_PROFILE <<END
ZOO_HOME=$ZOO_HOME
## PATH=/usr/lib64/qt-3.3/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
PATH=\$PATH:\$ZOO_HOME/bin:\$ZOO_HOME/conf
export ZOO_HOME PATH
END

##修改文件权限
chmod 755 $ZOO_PROFILE
##刷新环境变量
source /etc/profile

## 设置配置文件
cp $ZOO_VERSION/zoo.cfg $ZOO_HOME/conf
## 替换变量
sed -i "s@\${dataDir}@$ZOO_DATA@g" $ZOO_HOME/conf/zoo.cfg
sed -i "s@\${logDir}@$ZOO_LOGS@g" $ZOO_HOME/conf/zoo.cfg

## 重新设置目录的拥有者
chown hadoop:hadoop /home/hadoop -R
mkdir -p $ZOO_DATA;chown hadoop:hadoop /home/data -R
mkdir -p $ZOO_LOGS;chown hadoop:hadoop /home/logs -R
echo 0 > $ZOO_DATA/myid

## 同步子节点

# node01
scp $ZOO_PROFILE root@node01:/etc/profile.d
scp -r $ZOO_HOME root@node01:/home/hadoop/src
ssh root@node01 "chown hadoop:hadoop /home/hadoop -R"
ssh root@node01 "mkdir -p $ZOO_DATA;chown hadoop:hadoop /home/data -R"
ssh root@node01 "mkdir -p $ZOO_LOGS;chown hadoop:hadoop /home/logs -R"
ssh root@node01 "echo 1 > $ZOO_DATA/myid"

sudo -u hadoop ssh hadoop@node01 "source /etc/profile;env"
sudo -u hadoop ssh hadoop@node01 "cd $ZOO_HOME/bin; ./zkServer.sh start"

# node02
su root
scp $ZOO_PROFILE root@node02:/etc/profile.d
scp -r $ZOO_HOME root@node02:/home/hadoop/src
ssh root@node02 "chown hadoop:hadoop /home/hadoop -R"
ssh root@node02 "mkdir -p $ZOO_DATA;chown hadoop:hadoop /home/data -R"
ssh root@node02 "mkdir -p $ZOO_LOGS;chown hadoop:hadoop /home/logs -R"
ssh root@node02 "echo 1 > $ZOO_DATA/myid"

sudo -u hadoop ssh hadoop@node02 "source /etc/profile;env"
sudo -u hadoop ssh hadoop@node02 "cd $ZOO_HOME/bin; ./zkServer.sh start"
