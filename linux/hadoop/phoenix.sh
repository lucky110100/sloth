#!/bin/bash
set -e
## 读取配置文件
. ../config/my-config.sh

## 重新设置下载链接
PHOENIX_URL=$URL_FILE/hadoop
## 如果文件不存在，则下载
if [ ! -f "$PHOENIX_FILE" ]
then
    wget $PHOENIX_URL/$PHOENIX_FILE
fi
rm -rf $PHOENIX_HOME
tar -zxvf $PHOENIX_FILE -C ${HADOOP_DIR_OWN}/src


echo '查看hbase集群信息'
if [ -f 'hbase.info'  ];then
	chmod a+x hbase.info
	cat hbase.info
else
	touch hbase.info
fi

read -p "是否继续(Y/N)？：" isY
if [ "${isY}" != "y" ] && [ "${isY}" != "Y" ];then
   exit 1
fi

## 生成临时脚本文件
rm -f tmp.sh
echo "set -e" > tmp.sh

echo "分发文件到各个节点"
while read line
do 
    eval $(echo $line | awk '{print "i1="$1";i2="$2";i3="$3}')
    echo "$i2"
    echo "ssh root@$i3 'chown -R hadoop:hadoop $HBASE_HOME'" >> tmp.sh
    scp $PHOENIX_HOME/phoenix-4.8.0-HBase-0.98-server.jar root@${i3}:$HBASE_HOME/lib
done < hbase.info

echo "执行临时脚本"
chmod a+x tmp.sh;./tmp.sh