#!/bin/bash
set -e
## 检查输入参数
if [ $# -lt 1 ]; then
    echo "$0 错误：请传入参数，(1、服务类型:master/slave)！"
	echo "例如：$0 master"
	exit 2
fi

#######################	2.安装hadoop 	#######################
## 读取配置文件
. ../config/my-config.sh
## 重新设置下载链接
HIVE_URL=$URL_FILE/hadoop
mkdir -p $HIVE_HOME

## 如果文件不存在，则下载
if [ ! -f "$HIVE_FILE" ]
then
    wget $HIVE_URL/$HIVE_FILE
    ## tar -zxvf 解压并列出详情; -C 指定目录
	## tar -zxvf $HIVE_FILE -C /home/soft
fi
rm -rf $HIVE_HOME
tar -zxvf $HIVE_FILE -C /home/hadoop/src
##写入环境变量 
HIVE_PROFILE=$SYS_PROFILE_DIR/hive.sh

## 块输入，转义字符"\"
cat > $HIVE_PROFILE <<END
HIVE_HOME=$HIVE_HOME
## PATH=/usr/lib64/qt-3.3/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
PATH=\$PATH:\$HIVE_HOME/bin
export HIVE_HOME PATH
END

##修改文件权限
chmod 755 $HIVE_PROFILE
##刷新环境变量
source /etc/profile


## 设置配置文件
cp $HIVE_VERSION/hive-env.sh $HIVE_HOME/conf
cp $HIVE_VERSION/hive-site.xml $HIVE_HOME/conf
cp $HIVE_VERSION/hive-log4j.properties $HIVE_HOME/conf
cp mysql-connector-java-5.1.21-bin.jar $HIVE_HOME/lib
## 替换变量
sed -i "s@# HADOOP_HOME=\${bin}/../../hadoop@HADOOP_HOME=${HADOOP_HOME}@g" $HIVE_HOME/conf/hive-env.sh
sed -i "s@# export HIVE_CONF_DIR=@export HIVE_CONF_DIR=$HIVE_HOME/conf@g" $HIVE_HOME/conf/hive-env.sh
#sed -i "s@# export HIVE_AUX_JARS_PATH=@export HIVE_AUX_JARS_PATH==$HIVE_HOME/lib@g" $HIVE_HOME/conf/hive-env.sh

sed -i "s@\${jdbcURL}@jdbc:mysql://172.16.1.118:13326/hive?useUnicode=true\&amp;characterEncoding=utf-8@g" $HIVE_HOME/conf/hive-site.xml
sed -i "s@\${jdbcDriver}@com.mysql.jdbc.Driver@g" $HIVE_HOME/conf/hive-site.xml
sed -i "s@\${dbUser}@${dbUser1}@g" $HIVE_HOME/conf/hive-site.xml
sed -i "s@\${dbPwd}@${dbUser1}^0^@g" $HIVE_HOME/conf/hive-site.xml

sed -i "s@\${hiveLog}@/hom/logs/hive@g" $HIVE_HOME/conf/hive-log4j.properties
## 创建元数据库

## 重新设置目录的拥有者
chown hadoop:hadoop /home/hadoop -R
## 替换包
mv $HADOOP_HOME/share/hadoop/yarn/lib/jline-0.9.94.jar /home/backup/jline-0.9.94.jar
cp -f $HIVE_VERSION/lib/jline-2.12.jar $HADOOP_HOME/share/hadoop/yarn/lib

mv /home/hadoop/src/hadoop-2.3.0/share/hadoop/yarn/lib/jline-0.9.94.jar /home/backup/jline-0.9.94.jar
cp -f /home/hadoop/src/apache-hive-1.2.1-bin/lib/jline-2.12.jar /home/hadoop/src/hadoop-2.3.0/share/hadoop/yarn/lib
