#!/bin/bash
set -e
## 检查输入参数
if [ $# -lt 1 ]; then
    echo "$0 错误：请传入参数，(1、服务ID; 2、服务类型:master/slave)！"
	echo "例如：./$0 1 master 或 ./$0 1 master"
	exit 2
fi
## 读取配置文件
. ../config/my-config.sh

if [ ! -f "$MYSQL_FILE" ]
then
## 重新设置下载链接
	MYSQL_URL=$URL_FILE/mysql
	
##1.下载安装包，并上传到服务器；
	wget $MYSQL_URL/$MYSQL_FILE
fi

##2. 解压安装包到指定目录，修改文件的读写执行权限；
rm -rf $MYSQL_HOME
rm -rf $MYSQL_DATA
tar -zxvf $MYSQL_FILE -C /home

## 新增用户组和用户
#create user if not exists  
#egrep "^mysql" /etc/passwd >& /dev/null  
if !(egrep "^mysql" /etc/passwd >& /dev/null)
then  
    groupadd mysql
	useradd -g mysql mysql
fi

#改变当前目录的拥有者和用户组
chown -R mysql $MYSQL_HOME
chgrp -R mysql $MYSQL_HOME

##3. 编译源文件，并进行安装；
# 已经编译过的，解压可用

##4. 修改软件的相关配置文件，本地化或优化配置；可从support-files/my-default.cnf获取
## 向配置文件中输入基本信息
cat > /etc/my.cnf <<END
[mysqld]
basedir = $MYSQL_HOME
datadir = $MYSQL_DATA
port = $MYSQL_PORT
server_id = $1
# socket = .....

sql_mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES
## 修改临时表大小，提高查询速度
tmp_table_size=300M
max_heap_table_size=500M
## 最大连接数
max_connections=768
## 表名大小写不敏感
lower_case_table_names=1

## 日志配置
log_error=$MYSQL_DATA/mysql.err
#log=$MYSQL_DATA/mysql.log
slow-query-log=on
slow-query-log-file=$MYSQL_DATA/mysql.slow
long_query_time=3
END

## 写入初始化文件
cat > init.sql <<END
CREATE USER if not exists '${dbUser1}'@'%' IDENTIFIED BY '${dbPwd1}';
GRANT All ON *.* TO '${dbUser1}'@'%';
CREATE USER if not exists '${dbUser1}'@'localhost' IDENTIFIED BY '${dbPwd1}';
GRANT All ON *.* TO '${dbUser1}'@'localhost';
flush privileges;
create database if not exists bufferdb character set = 'utf8' collate = 'utf8_general_ci';
END

## 主从选择
if [ "$2" == "master" ];then
	cat >> /etc/my.cnf <<END
## 主从配置-主库
log-bin=mysql-bin
## 日志的过期天数
expire_logs_days = 7
## 需要同步的数据库
#binlog-do-db=db1 
#binlog-do-db=db2
#binlog-do-db=db3
## 不需要同步的数据库
#binlog-ignore-db=mysql
END
	echo  "GRANT REPLICATION SLAVE ON *.* to '${mySyncUser}'@'%' identified by '${mySyncPwd}'; " >> init.sql
elif [ "$2" == "slave" ];then
	cat >> /etc/my.cnf <<END
## 主从配置-从库
log_bin = mysql-bin
expire_logs_days = 7
relay_log = mysql-relay-bin
log_slave_updates = 1
read_only = 1
## 需要复制的库
#replicate-do-db=db1
## 不需要复制的库
#replicate-ignore-db=mysql
END
	cat >> init.sql <<END
STOP SLAVE;
CHANGE MASTER TO MASTER_HOST='${hosts[0]}', MASTER_PORT=${MYSQL_PORT}, MASTER_USER='${mySyncUser}', MASTER_PASSWORD='${mySyncPwd}', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=154, MASTER_CONNECT_RETRY=60;
START SLAVE;
END
	## 删除从库的uuid，否则启动从服务报错 because master and slave have equal MySQL server UUIDs;
	rm -f $MYSQL_HOME/data/auto.cnf
fi

##5. 把软件注册为服务，并随系统启动；可从support-files/mysql.server获取
cp -f $MYSQL_INID /etc/init.d/$MYSQL_INID
chmod a+x /etc/init.d/$MYSQL_INID
sed -i "s#@{myhome}#$MYSQL_HOME#g" /etc/init.d/$MYSQL_INID
sed -i "s#@{mydata}#$MYSQL_DATA#g" /etc/init.d/$MYSQL_INID
chkconfig $MYSQL_INID on

##6. 修改用户环境变量，或建立软链接，方便快速调用软件命令；
ln -sf $MYSQL_HOME/bin/mysql /usr/bin

##7. 修改防火墙配置，以便外部访问；
../common/change-iptables.sh $MYSQL_PORT

##8. 设置监控日志
ln -sf $MYSQL_HOME/data/mysql.err $HOME_LOG
ln -sf $MYSQL_HOME/data/mysql.slow $HOME_LOG

## 启动定时日志清理和备份方案
#cp -f haha.cnf /home/backup/haha
#cp -f haha.cnf /home/backup/haha

## 启动数据库，并执行初始化脚本
service mysqld restart
## 无密码的
mysql -u root --auto-rehash --line-numbers --show-warnings mysql < init.sql
