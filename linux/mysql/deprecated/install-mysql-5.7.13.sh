#!/bin/bash
set -e
## 读取配置文件
. ../config/my-config.sh

## 重新设置下载链接
MYSQL_URL=$URL_FILE/mysql

## 解压文件
wget $MYSQL_URL/mysql-5.7.13-linux-glibc2.5-x86_64.tar.gz
tar -zxvf mysql-5.7.13-linux-glibc2.5-x86_64.tar.gz -C /home
mv -f /home/mysql-5.7.13-linux-glibc2.5-x86_64 $MYSQL_HOME

#新增用户组和用户
groupadd mysql
useradd -g mysql mysql

#改变当前目录的拥有者和用户组
chown -R mysql $MYSQL_HOME
chgrp -R mysql $MYSQL_HOME

## 初始化数据库(--initialize-insecure不设置root密码的初始化)
$MYSQL_HOME/bin/mysqld --initialize-insecure --user=mysql --basedir=$MYSQL_HOME --datadir=$MYSQL_DATA

##4. 修改软件的相关配置文件，本地化或优化配置；可从support-files/my-default.cnf获取
cp -f $MYSQL_CNF /etc/my.cnf
sed -i "s#@{myhome}#$MYSQL_HOME#g" /etc/my.cnf
sed -i "s#@{mydata}#$MYSQL_DATA#g" /etc/my.cnf
sed -i "s#@{myport}#$MYSQL_PORT#g" /etc/my.cnf
sed -i "s#@{serverid}#1#g" /etc/my.cnf

##5. 把软件注册为服务，并随系统启动；可从support-files/mysql.server获取
cp -f $MYSQL_INID /etc/init.d/$MYSQL_INID
chmod a+x /etc/init.d/$MYSQL_INID
sed -i "s#@{myhome}#$MYSQL_HOME#g" /etc/init.d/$MYSQL_INID
sed -i "s#@{mydata}#$MYSQL_DATA#g" /etc/init.d/$MYSQL_INID
chkconfig $MYSQL_INID on

##6. 修改用户环境变量，或建立软链接，方便快速调用软件命令；
ln -sf $MYSQL_HOME/bin/mysql /usr/bin

##7. 修改防火墙配置，以便外部访问；
../common/change-iptables.sh $MYSQL_PORT

##8. 设置监控日志
ln -sf $MYSQL_HOME/data/mysql.err $HOME_LOG
ln -sf $MYSQL_HOME/data/mysql.slow $HOME_LOG

## 启动定时日志清理和备份方案
#cp -f haha.cnf /home/backup/haha
#cp -f haha.cnf /home/backup/haha

## 启动数据库，并执行初始化脚本
service mysqld start
## 无密码的
mysql -u root --auto-rehash --line-numbers --show-warnings mysql < default-init.sql
## 有密码的
#mysql -u root -p123456 --auto-rehash --line-numbers --show-warnings mysql < default-init.sql

##9. 能够安全干净删除已安装软件，并重新安装
##echo 请编写卸载脚本！
