#!/bin/bash
set -e
mkdir -p /home/ftp
useradd -g ftp -d /home/ftp -s /sbin/nologin lutong
passwd lutong
#lutong2016data

#开启PASV模式
vi /etc/vsftpd/vsftpd.conf
#在文件后面添加 
listen_port=40000
pasv_enable=YES
pasv_min_port=40000 
pasv_max_port=40080
# pasv_promiscuous=YES

vi /etc/sysconfig/iptables
#添加
-A INPUT -m state --state NEW -m tcp -p -dport 40000:40080 -j ACCEPT

#重启iptabls和vsftpd
service iptables restart
service vsftpd restart

#允许切换到用户宿主目录
setsebool -P ftp_home_dir  on
setsebool -P allow_ftpd_full_access on
# 添加用户
adduser -d /home/install -g ftp -s /sbin/nologin demo

adduser -d /home/ltdata -g ftp -s /sbin/nologin ltdata
passwd ltdata
lt2016

adduser -d /home/install -g ftp -s /sbin/nologin demo
echo '123456'| passwd --stdin demo

##只允许访问家目录
vi /etc/vsftpd/chroot_list
