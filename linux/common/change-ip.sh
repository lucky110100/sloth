#!/bin/bash
TODAY=$(date +%Y-%m-%d)
BACKUP_DIR=/home/backup
echo $TODAY
echo $BACKUP_DIR
##修改文件的执行权限
#chmod a+x change-ip.sh
##查看当前网络配置
# ifconfig -a
##网卡配置文件地址，eth0为旧的命名方式，em1为新的命名方式，可通过ls /etc/sysconfig/network-scripts查看
IP_PATH=/etc/sysconfig/network-scripts/ifcfg-eth0
## 备份配置文件
cp -f $IP_PATH $BACKUP_DIR/ifcfg-eth0-$TODAY
##准备写入文件，">"为重写，">>"为追加
echo "DEVICE=eth0"					>		$IP_PATH
echo "BOOTPROTO=static"				>>		$IP_PATH
echo "HWADDR=B8:2A:72:DF:E7:E0"		>>		$IP_PATH
echo "ONBOOT=yes"					>>		$IP_PATH
echo "TYPE=Ethernet"				>>		$IP_PATH
echo "IPADDR=172.16.1.123"			>>		$IP_PATH
echo "NETMASK=255.255.0.0"			>>		$IP_PATH
echo "GATEWAY=172.16.0.1"			>>		$IP_PATH
echo "DNS1=202.96.134.133"			>>		$IP_PATH
echo "DNS2=8.8.4.4"					>>		$IP_PATH
echo "NM_CONTROLLED=yes"			>>		$IP_PATH
#echo "UUID=d7457cec-8e9b-49af-b612-2802a68dc6b1"		>>		$IP_PATH
## 重启网卡
#/etc/init.d/network restart
service network restart
## 一个可能有问题的地方，删除或修改
#rm -rf /etc/udev/rules.d/70-persistent-net.rules


